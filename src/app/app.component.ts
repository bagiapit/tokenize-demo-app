import { Component } from '@angular/core';
import { io } from "socket.io-client";

import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  private socket: any;

  bids: any = [];
  asks: any = [];
  totalOfSize = 0;
  totalOfAmount = 0;

  constructor() {
    this.socket = io(environment.webSocketUrl);
  }

  ngOnInit() {
    this.socket.emit('notification', 'Message from client with ♥: Hi WebSocket ^^');
    this.socket.on('notification', (res: any) => {
      const { data } = res;
      const { bids, asks, totalOfAmount, totalOfSize } = data;
      this.bids = bids;
      this.asks = asks;
      this.totalOfAmount = totalOfAmount.toFixed(8);
      this.totalOfSize = totalOfSize.toFixed(8);
    });
  }

}
